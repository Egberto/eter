<div align="center">

[Fuerzas Etéreas][0]
====================

![](promo/icono.png "Logo preliminar")

![](https://img.shields.io/badge/Versión-Alpha-blue.svg "Etapa muy temprana de desarrollo")
![](https://img.shields.io/badge/Software-Libre-blue.svg "Las 4 libertades")
[![][1]][2]
[![][3]][4]
[![][5]][6]
[![][7]][8]
[![][9]][10]

[0]: https://egberto.codeberg.page/Fuerzas_Et%C3%A9reas.html "Página del proyecto"
[1]: https://img.shields.io/badge/Licencia-GPLv3-blue.svg "Licencia Pública General 3.0"
[2]: https://www.gnu.org/licenses/gpl-3.0.en.html
[3]: https://img.shields.io/badge/Godot-4-blue.svg "Godot Game Engine"
[4]: https://godotengine.org
[5]: https://img.shields.io/badge/Linux-FCC624?logo=linux&logoColor=black "Desarrollo en"
[6]: https://www.gnu.org
[7]: https://img.shields.io/liberapay/patrons/EgbertoFuentes?logo=liberapay "Donar"
[8]: https://liberapay.com/EgbertoFuentes/donate
[9]: https://img.shields.io/mastodon/follow/193680?domain=https%3A%2F%2Fmstdn.mx&label=%40EgbertoFuentes
"Sigueme en el Fediverso"
[10]: https://mstdn.mx/@EgbertoFuentes

</div>

*****

Descripción
-----------

Videojuego de Rol que será de un solo jugador y basado en turnos.

Capturas de pantalla
--------------------
![](./README/crea-personaje.png)

![](./README/vista-global.png)

Requerimientos
--------------

Ejecutará en sistemas de escritorio como:


* **GNU/Linux**, **Windows** o **macOS**.


Para su compilación necesitará:


* **Godot Engine** (versión 4).


