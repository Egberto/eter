extends Control

const RUTA_JUGADOR = "res://creación/jugador/"
const RUTA_JUGADORA = "res://creación/jugadora/"
const MASCULINO = 0
const FEMENINO = 1

var masculino = ["brazo_d", "piernas", "torso", "calzón", "cara", "esclera",
"iris", "párpados", "nariz", "boca", "cejas", "barba", "pelo", "mano_d",
"brazo_i", "mano_i"]
var femenino = ["brazo_d", "piernas", "torso", "calzón", "sostén", "cara",
"esclera", "iris", "párpados", "nariz", "boca", "cejas", "pelo", "mano_d",
"brazo_i", "mano_i"]
var pieles = ["brazo_d", "piernas", "torso", "cara", "párpados", "nariz",
"boca", "mano_d", "brazo_i", "mano_i"]
var pelos = ["cejas", "barba", "pelo"]
var ordenaciones = [masculino, femenino]
var datos = []
var sexo = MASCULINO
var tonos_pelo
var tonos_piel
var tonos_ojos

@onready var btn_masculino = $Fondo/C/ColPrincipal/RenPrincipal/ColDatos/Sexos/Masculino
@onready var btn_femenino = $Fondo/C/ColPrincipal/RenPrincipal/ColDatos/Sexos/Femenino
@onready var ctn_tonos_pelo = $Fondo/C/ColPrincipal/RenPrincipal/ColDatos/TonosPelo
@onready var ctn_tonos_piel = $Fondo/C/ColPrincipal/RenPrincipal/ColDatos/TonosPiel
@onready var ctn_tonos_ojos = $Fondo/C/ColPrincipal/RenPrincipal/ColDatos/TonosOjos
@onready var sld_cara = $Fondo/C/ColPrincipal/RenPrincipal/ColOpciones/OpCara
@onready var sld_ojos = $Fondo/C/ColPrincipal/RenPrincipal/ColOpciones/OpOjos
@onready var sld_nariz = $Fondo/C/ColPrincipal/RenPrincipal/ColOpciones/OpNariz
@onready var sld_boca = $Fondo/C/ColPrincipal/RenPrincipal/ColOpciones/OpBoca
@onready var sld_cejas = $Fondo/C/ColPrincipal/RenPrincipal/ColOpciones/OpCejas
@onready var sld_pelo = $Fondo/C/ColPrincipal/RenPrincipal/ColOpciones/OpPelo
@onready var sld_barba = $Fondo/C/ColPrincipal/RenPrincipal/ColOpciones/OpBarba
@onready var chb_cejas = $Fondo/C/ColPrincipal/RenPrincipal/ColOpciones/Cejas
@onready var chb_pelo = $Fondo/C/ColPrincipal/RenPrincipal/ColOpciones/Pelo
@onready var chb_barba = $Fondo/C/ColPrincipal/RenPrincipal/ColOpciones/Barba
@onready var avatar = $"Posición/Avatar"


func carga_todo() -> void:
	var rutas = [RUTA_JUGADOR, RUTA_JUGADORA]
	for sxo in ordenaciones.size():
		datos.append([])
		var archivos = Global.carga_lista_archivos(rutas[sxo], "*.tscn")
		archivos.sort()
		for parte in ordenaciones[sxo].size():
			datos[sxo].append([])
			for el in archivos:
				if el.match(ordenaciones[sxo][parte] + "*"):
					var obj = load(rutas[sxo] + el).instantiate()
					datos[sxo][parte].append(obj)


func actualiza_deslizante(nodo, parte) -> void:
	nodo.ticks_on_borders = true
	nodo.tick_count = dame_cantidad(parte)
	nodo.max_value = nodo.tick_count - 1
	nodo.value = 0


func actualiza_sexo() -> void:
	sexo = MASCULINO if btn_masculino.is_pressed() else FEMENINO
	for nodo in avatar.get_children():
		avatar.remove_child(nodo)
	for parte in datos[sexo]:
		avatar.add_child(parte[0])
	actualiza_deslizante(sld_cara, "cara")
	actualiza_deslizante(sld_ojos, "párpados")
	actualiza_deslizante(sld_nariz, "nariz")
	actualiza_deslizante(sld_boca, "boca")
	actualiza_deslizante(sld_cejas, "cejas")
	actualiza_deslizante(sld_pelo, "pelo")
	actualiza_deslizante(sld_barba, "barba")
	chb_cejas.set_pressed_no_signal(true)
	chb_pelo.set_pressed_no_signal(true)
	chb_barba.set_pressed_no_signal(true if sexo == MASCULINO else false)
	for nodo in get_tree().get_nodes_in_group("barba"):
		nodo.visible = true if sexo == MASCULINO else false
	cambia_color_pelo(tonos_pelo[3].color)
	cambia_color_piel(tonos_piel[3].color)
	cambia_color("iris", tonos_ojos[3].color)


func actualiza_pelos() -> void:
	for nombre_parte in pelos:
		var nodo = dame_nodo_avatar(nombre_parte)
		if nodo:
			avatar.remove_child(nodo)
	var nodo_previo = avatar.find_child("Boca*", false, false)
	var activados = [chb_cejas, chb_barba, chb_pelo]
	var deslizantes = [sld_cejas, sld_barba, sld_pelo]
	var i = 0
	for nombre_parte in pelos:
		if activados[i].button_pressed:
			deslizantes[i].editable = true
			var parte = dame_índice_parte(nombre_parte)
			var nodo = datos[sexo][parte][deslizantes[i].value]
			nodo_previo.add_sibling(nodo)
			nodo_previo = nodo
		else:
			deslizantes[i].editable = false
		i += 1


func dame_índice_parte(nombre_parte: String) -> int:
	return ordenaciones[sexo].find(nombre_parte)


func dame_nodo_avatar(nombre_parte: String) -> Node:
	var nombre_nodo = nombre_parte.to_pascal_case() + "*"
	return avatar.find_child(nombre_nodo, false, false)


func dame_cantidad(nombre_parte: String) -> int:
	var parte = dame_índice_parte(nombre_parte)
	if parte != -1:
		return datos[sexo][parte].size()
	else:
		return 0


func cambia_parte(nombre_parte: String, valor: int) -> void:
	var parte = dame_índice_parte(nombre_parte)
	if parte != -1:
		var nodo = dame_nodo_avatar(nombre_parte)
		if nodo:
			nodo.replace_by(datos[sexo][parte][valor])


func cambia_color(nombre_parte, color) -> void:
	var parte = dame_índice_parte(nombre_parte)
	if parte != -1:
		for el in datos[sexo][parte]:
			el.self_modulate = color


func cambia_color_pelo(color) -> void:
	for el in pelos:
		cambia_color(el, color)


func cambia_color_piel(color) -> void:
	for el in pieles:
		cambia_color(el, color)


func _ready():
	tonos_pelo = ctn_tonos_pelo.get_children()
	tonos_piel = ctn_tonos_piel.get_children()
	tonos_ojos = ctn_tonos_ojos.get_children()
	carga_todo()
	actualiza_sexo()


func _on_OpPelo_value_changed(value):
	cambia_parte("pelo", value)


func _on_OpCara_value_changed(value):
	cambia_parte("cara", value)


func _on_OpOjos_value_changed(value):
	cambia_parte("párpados", value)


func _on_OpCejas_value_changed(value):
	cambia_parte("cejas", value)


func _on_OpNariz_value_changed(value):
	cambia_parte("nariz", value)


func _on_OpBarba_value_changed(value):
	cambia_parte("barba", value)


func _on_OpBoca_value_changed(value):
	cambia_parte("boca", value)


func _on_TonoP1_gui_input(event):
	if event.is_pressed():
		cambia_color_pelo(tonos_pelo[0].color)


func _on_TonoP2_gui_input(event):
	if event.is_pressed():
		cambia_color_pelo(tonos_pelo[1].color)


func _on_TonoP3_gui_input(event):
	if event.is_pressed():
		cambia_color_pelo(tonos_pelo[2].color)


func _on_TonoP4_gui_input(event):
	if event.is_pressed():
		cambia_color_pelo(tonos_pelo[3].color)


func _on_TonoP5_gui_input(event):
	if event.is_pressed():
		cambia_color_pelo(tonos_pelo[4].color)


func _on_TonoP6_gui_input(event):
	if event.is_pressed():
		cambia_color_pelo(tonos_pelo[5].color)


func _on_TonoP7_gui_input(event):
	if event.is_pressed():
		cambia_color_pelo(tonos_pelo[6].color)


func _on_Tono1_gui_input(event):
	if event.is_pressed():
		cambia_color_piel(tonos_piel[0].color)


func _on_Tono2_gui_input(event):
	if event.is_pressed():
		cambia_color_piel(tonos_piel[1].color)


func _on_Tono3_gui_input(event):
	if event.is_pressed():
		cambia_color_piel(tonos_piel[2].color)


func _on_Tono4_gui_input(event):
	if event.is_pressed():
		cambia_color_piel(tonos_piel[3].color)


func _on_Tono5_gui_input(event):
	if event.is_pressed():
		cambia_color_piel(tonos_piel[4].color)


func _on_Tono6_gui_input(event):
	if event.is_pressed():
		cambia_color_piel(tonos_piel[5].color)


func _on_Tono7_gui_input(event):
	if event.is_pressed():
		cambia_color_piel(tonos_piel[6].color)


func _on_TonoO1_gui_input(event):
	if event.is_pressed():
		cambia_color("iris", tonos_ojos[0].color)


func _on_TonoO2_gui_input(event):
	if event.is_pressed():
		cambia_color("iris", tonos_ojos[1].color)


func _on_TonoO3_gui_input(event):
	if event.is_pressed():
		cambia_color("iris", tonos_ojos[2].color)


func _on_TonoO4_gui_input(event):
	if event.is_pressed():
		cambia_color("iris", tonos_ojos[3].color)


func _on_TonoO5_gui_input(event):
	if event.is_pressed():
		cambia_color("iris", tonos_ojos[4].color)


func _on_TonoO6_gui_input(event):
	if event.is_pressed():
		cambia_color("iris", tonos_ojos[5].color)


func _on_TonoO7_gui_input(event):
	if event.is_pressed():
		cambia_color("iris", tonos_ojos[6].color)


func _on_masculino_toggled(button_pressed):
	btn_femenino.button_pressed = not button_pressed
	actualiza_sexo()


func _on_femenino_toggled(button_pressed):
	btn_masculino.button_pressed = not button_pressed
	actualiza_sexo()


func _on_cejas_toggled(_button_pressed):
	actualiza_pelos()


func _on_pelo_toggled(_button_pressed):
	actualiza_pelos()


func _on_barba_toggled(_button_pressed):
	actualiza_pelos()


func _on_aceptar_pressed():
	get_tree().quit()


func _on_cancelar_pressed():
	Global.carga_escena("res://presentación/menú_principal.tscn")
