extends Node

const RUTA_ESCENA_CARGA : StringName = &"res://interfaz/pantalla_carga.tscn"

var parám_escena = null


func carga_escena(ruta) -> void:
	parám_escena = ruta
	get_tree().change_scene_to_file(RUTA_ESCENA_CARGA)


func carga_lista_archivos(ruta, patrón) -> Array:
	var dir = DirAccess.open(ruta)
	var lst = []
	if dir:
		dir.list_dir_begin()
		var nombre_archivo = dir.get_next()
		while nombre_archivo != "":
			if not dir.current_is_dir() and nombre_archivo.match(patrón):
				lst.append(nombre_archivo)
			nombre_archivo = dir.get_next()
	else:
		print("No se pudo acceder al directorio: ", ruta)
	return lst
