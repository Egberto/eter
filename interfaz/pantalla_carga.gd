extends Control


@onready var barra_progreso = $Margenes/BarraProgreso
@onready var aviso_error = $AvisoError
@onready var ruta_escena : String = Global.parám_escena

var error = null


func muestra_error(mensaje):
	aviso_error.title = mensaje
	aviso_error.show()


func _ready() -> void:
	if ruta_escena:
		ResourceLoader.load_threaded_request(ruta_escena)
	else:
		error = "¡Ruta vacía!"


func _process(_delta) -> void:
	if error:
		muestra_error(error)
		return
	var progreso = []
	var estado = ResourceLoader.load_threaded_get_status(ruta_escena, progreso)
	match estado:
		ResourceLoader.THREAD_LOAD_IN_PROGRESS:
			barra_progreso.value = progreso[0]
		ResourceLoader.THREAD_LOAD_FAILED:
			muestra_error("¡Falló la carga!")
		ResourceLoader.THREAD_LOAD_LOADED:
			get_tree().change_scene_to_packed(ResourceLoader.load_threaded_get(ruta_escena))
