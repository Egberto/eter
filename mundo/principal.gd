extends Node2D

@export var rapidez : float = 30.0
@export var escala_tiempo : float = 0.05
@export var color_noche : Color = Color8(30, 45, 155)

var ruta = []
var luces = []
var medida_pantalla : Vector2

@onready var jugador : CharacterBody2D = $CapaJugador/Jugador
@onready var camara : Camera2D = $CapaJugador/Jugador/Camara
@onready var navegante : NavigationAgent2D = $CapaJugador/Jugador/AgenteNav
@onready var destino : Sprite2D = $CapaJugador/Destino
@onready var mar : Sprite2D = $CapaMar/Mar
@onready var nubes : Sprite2D = $CapaNubes/Nubes
@onready var sombra : Sprite2D = $CapaNubes/Sombra
@onready var modulador : CanvasModulate = $Modulador
@onready var modulador_mar : CanvasModulate = $CapaMar/ModuladorMar
@onready var modulador_luces : CanvasModulate = $CapaLuces/ModuladorLuces
@onready var modulador_nubes : CanvasModulate = $CapaNubes/ModuladorNubes
@onready var modulador_jugador : CanvasModulate = $CapaJugador/ModuladorJugador
@onready var círculo_día : Sprite2D= $"IU/Brújula/CírculoDía"
@onready var confirma_salir : ConfirmationDialog = $"IU/ConfirmaSalir"


func ajusta_en_capa(sprite) -> void:
	var ajuste_aspecto = Vector2.ZERO;
	var textura : Texture2D = sprite.get_texture()
	var ancho : float = medida_pantalla.x
	var alto : float = medida_pantalla.y

	ajuste_aspecto.x = ancho / textura.get_width()
	ajuste_aspecto.y = alto / textura.get_height()
	sprite.position = Vector2(0, 0)
	sprite.scale = ajuste_aspecto
	sprite.material.set_shader_parameter("ajuste_aspecto", ajuste_aspecto)


func _ready() -> void:
	medida_pantalla = DisplayServer.screen_get_size()
	ajusta_en_capa(mar)
	ajusta_en_capa(nubes)
	ajusta_en_capa(sombra)
	navegante.path_desired_distance = 4.0
	navegante.target_desired_distance = 4.0
	luces = $CapaLuces.get_children()
	call_deferred("prepara_jugador")


func prepara_jugador() -> void:
	await get_tree().physics_frame
	jugador.set_position($Inicio.get_position())
	await get_tree().physics_frame
	fija_destino($Inicio.get_position())


func fija_destino(pdestino: Vector2) -> void:
	var destino_previo : Vector2 = navegante.get_target_position()
	
	navegante.set_target_position(pdestino)
	if navegante.is_target_reachable():
		destino.set_position(pdestino)
		destino.show()
		$Pasos.play()
	else:
		$Inaccesible.play()
		navegante.set_target_position(destino_previo)


func _process(_delta) -> void:
	var movimiento : Vector2 = camara.global_position / medida_pantalla
	var tono : float = absf(sin(círculo_día.rotation * 0.5))
	var tono_inverso : float = 1.0 - tono
	var desplazamiento : Vector2 = Vector2(0.05, -0.15)

	mar.material.set_shader_parameter("movimiento", movimiento)
	nubes.material.set_shader_parameter("movimiento", movimiento)
	sombra.material.set_shader_parameter("movimiento", movimiento + desplazamiento)
	modulador.color = color_noche.blend(Color.WHITE * tono)
	modulador_mar.color = modulador.color
	modulador_nubes.color = modulador.color
	modulador_jugador.color = modulador.color
	modulador_luces.color.a = tono_inverso


func _physics_process(_delta) -> void:
	if navegante.is_target_reached():
		destino.hide()
		$Pasos.stop()
		return

	var posicion_actual : Vector2 = jugador.global_transform.origin
	var posicion_siguiente : Vector2 = navegante.get_next_path_position()
	var direccion : Vector2 = (posicion_siguiente - posicion_actual).normalized()

	jugador.set_velocity(direccion * rapidez)
	jugador.move_and_slide()
	círculo_día.rotation_degrees += escala_tiempo


func _unhandled_input(event) -> void:
	if event.is_action_pressed("ui_cancel"):
		confirma_salir.show()
	if event.is_action_pressed("clic_primario"):
		fija_destino(get_global_mouse_position())
