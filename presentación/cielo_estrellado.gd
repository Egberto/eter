extends ParallaxBackground

@export var velocidad_cielo := 25.0

func _process(delta) -> void:
	scroll_offset.x += velocidad_cielo * delta
