extends Control

const ruta_escena_carga = "res://interfaz/pantalla_carga.tscn"
const ruta_escena_crea_jugador = "res://creación/crea_jugador.tscn"
var espada := preload("res://interfaz/tema/cursores/espada.png")
var guantelete := preload("res://interfaz/tema/cursores/guantelete.png")


func _ready() -> void:
	Input.set_custom_mouse_cursor(espada)
	Input.set_custom_mouse_cursor(guantelete, Input.CURSOR_POINTING_HAND, Vector2(10, 2))


func _unhandled_input(event) -> void:
	if event.is_action_pressed("ui_cancel"):
		$ConfirmaSalir.show()


func _on_continuar_pressed() -> void:
	Global.carga_escena("res://mundo/principal.tscn")


func _on_nuevo_pressed() -> void:
	Global.carga_escena("res://presentación/prefacio.tscn")


func _on_opciones_pressed():
	pass


func _on_salir_pressed():
	$ConfirmaSalir.show()
